import Vue from 'vue'
import Router from 'vue-router'

import Home  from '@/views/Login/Home.vue'
import Login from '@/views/Login/Login.vue'

import CatProductos from '@/views/clasificacion/arts/CatProductos.vue'
import ProducCli    from '@/views/clasificacion/arts/ProducCli.vue'
import NewProducto  from '@/views/clasificacion/arts/NewProducto.vue'
import VerArt       from '@/views/clasificacion/arts/VerArt.vue'

import CatEntradas from '@/views/clasificacion/entradas/CatEntradas.vue'
import NewEntrada  from '@/views/clasificacion/entradas/NewEntrada.vue'
import VerEntrada  from '@/views/clasificacion/entradas/VerEntrada.vue'

import CatSalidas  from '@/views/clasificacion/salidas/CatSalidas.vue'
import NewSalidas  from '@/views/clasificacion/salidas/NewSalidas.vue'

import CatExistencia from '@/views/clasificacion/existencias/CatExistencia.vue'
import VerExistencia from '@/views/clasificacion/existencias/VerExistencia.vue'
import Faltantes from '@/views/clasificacion/existencias/Faltantes.vue'

import CatDiscre from '@/views/clasificacion/discrepancias/CatDiscre.vue'

import CatLab   from '@/views/clasificacion/laboratorios/CatLab.vue'
import Categorias   from '@/views/clasificacion/Categorias.vue'

import Info  from '@/views/Info.vue'
import VueJwtDecode from 'vue-jwt-decode'
import store from '@/store'
import rutas from '@/router'


Vue.use(Router)

// export default new Router({

var router = new Router({    
  mode: '',
  base: process.env.BASE_URL,
  routes: [
    // { path: '/',     name: 'login', component: Login},
    // { path: '/home', name: 'home',  component: Home},

    //MODULO DE LOGIN
    { path: '/home', name: 'home' , component: Home, 
      meta: { ADMIN: true} },
    { path: '/'    , name: 'Login', component: Login , 
      meta: { libre: true }},
    

    //Artículos
    { path: '/produccli', name: 'produccli', component: ProducCli,
      meta: { ADMIN: true } }, 
    { path: '/catproductos', name: 'catproductos', component: CatProductos,
      meta: { ADMIN: true } },
    { path: '/newproducto', name: 'newproducto', component: NewProducto,
      meta: { ADMIN: true } },
    { path: '/verart', name: 'verart', component: VerArt,
      meta: { ADMIN: true } },

    { path: '/catentradas', name: 'catentradas', component: CatEntradas,
      meta: { ADMIN: true } },
    { path: '/newentrada', name: 'newentrada', component: NewEntrada,
      meta: { ADMIN: true } },
    { path: '/verentrada', name: 'verentrada', component: VerEntrada,
      meta: { ADMIN: true } },

    { path: '/catsalidas', name: 'catsalidas', component: CatSalidas,
      meta: { ADMIN: true } },
    { path: '/newsalidas', name: 'newsalidas', component: NewSalidas,
      meta: { ADMIN: true } },

    { path: '/catexistencia', name: 'catexistencia', component: CatExistencia,
      meta: { ADMIN: true } },
    { path: '/verexistencia', name: 'verexistencia', component: VerExistencia,
      meta: { ADMIN: true } },
    { path: '/faltantes', name: 'faltantes', component: Faltantes,
      meta: { ADMIN: true } },

    { path: '/catdiscre', name: 'catdiscre', component: CatDiscre,
      meta: { ADMIN: true } },

    { path: '/catlab', name: 'catlab', component: CatLab,
      meta: { ADMIN: true } },
    { path: '/info', name: 'info', component: Info,
      meta: { ADMIN: true } },

    { path: '/categorias', name: 'Categorias',  component: Categorias,
      meta: { ADMIN: true } },
    
  ]
})


router.beforeEach( (to, from, next) => {
  // infica a que ruta voy a acceder
  // matched.some = compara si el meta es libre
  if(to.matched.some(record => record.meta.libre)){
    next()
  }else if(store.state.Login.login == true){
    if(to.matched.some(record => record.meta.ADMIN)){
      next()
    }
  }else{
    next({
      name: 'Login'
    })
  }
})

export default router


// Usar este códigoc cuancdo halla niveles


// router.beforeEach( (to, from, next) => {
  
//   if (localStorage.getItem('tlaKey')) {
//     var cToken  = localStorage.tlaKey;
//     next()
//     // NO TIENE TOKEN. cHORIZO
//   }else{
//     localStorage.removeItem("tlaKey")
//     // localStorage.clear();
    
//      var token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1Njc2MTcwNjQsImlkIjoibWFudWVsQHNhaXQuY29tLm14Iiwib3JpZ19pYXQiOjE1Njc2MTM0NjR9.ZZlvQqjH6QX8xLpGm4kmW68NX8vGxC-NE6BWCYJeMYk';
//      localStorage.setItem('tlaKey', token);
//     //  // next ()
//     next({path: '/'});  // next({name: 'Login'});
//   }

//   if(to.matched.some(record => record.meta.libre)){
//     next()

//   }else if(store.state.usuario && store.state.nivel == "ADMIN"){

//     if(to.matched.some(record => record.meta.ADMIN)){
//       next()
//     }

//   }else if(store.state.usuario && store.state.nivel == "USER"){

//     if(to.matched.some(record => record.meta.USER)){
//       next()
//     }

//   }else{
//     //No hay nivel => Mando a Login
//     next({  name: 'Login' })
//   }
// })

// export default router 
