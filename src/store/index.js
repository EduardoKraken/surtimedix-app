
import Vue from 'vue'
import Vuex from 'vuex'
import VueJwtDecode from 'vue-jwt-decode'

// import rutas    from './router'
import router   from '@/router'

import Login     from '@/modules/Login/Login'
import productos from '@/modules/productos'
import  createPersistedState  from  'vuex-persistedstate'

Vue.use(Vuex,VueJwtDecode)

export default new Vuex.Store({
  state: {
  	
  },
  mutations: {

  },
  actions: {
  	
  },

  getters:{

	},

	modules:{
		Login,
		productos,
	},

	plugins: [createPersistedState()]

})

